package com.br.api.investimentos.controllers;

import com.br.api.investimentos.enums.TipoDeRisco;
import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.services.InvestimentosService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentosController.class)
public class InvestimentosControllerTest {
    Investimentos investimentos;
    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void Inicialize(){
        investimentos  = new Investimentos("itau rf", "investimentos de renda fixa", TipoDeRisco.BAIXO, 0.01);
    }
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    InvestimentosService investimentosService;

    @Test
    public void testarCriarInvestimento() throws Exception {
        Mockito.when(investimentosService.salvarInvestimentos(Mockito.any(Investimentos.class))).thenReturn(investimentos);

        String json = objectMapper.writeValueAsString(investimentos);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(investimentos.getNome())));
    }


    @Test
    public void testarCriarInvestimentoErro() throws Exception {
        investimentos.setPorcentagem(0.0);
        Mockito.when(investimentosService.salvarInvestimentos(Mockito.any(Investimentos.class))).thenReturn(investimentos);
        InvestimentosController investimentosController = new InvestimentosController();
        try {
            investimentosController.criarInvestimentos(investimentos);
        } catch (ResponseStatusException rse)
        {

        }

    }



    @Test
    public void testerBuscarInvestimento() throws Exception {

        Iterable<Investimentos> investimentosIterator = Arrays.asList(investimentos ,investimentos);
        Mockito.when(investimentosService.buscarTodoInvestimentos()).thenReturn(investimentosIterator);

        String json = objectMapper.writeValueAsString(investimentosIterator);

        System.out.println(investimentos.getNome());
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void testarDeleteInvestimentos() throws Exception {
        investimentos.setId(3);
        Optional<Investimentos> investimentosOptional = Optional.of(investimentos);
        Mockito.when(investimentosService.buscarPorId(Mockito.anyInt())).thenReturn(investimentosOptional);
        //Mockito.when(investimentosService.deletarInvestimentos(investimentosOptional.get())).getMock()
        //Mockito.verify(investimentosService.deletarInvestimentos(investimentos);


        String json = objectMapper.writeValueAsString(investimentos);


        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
