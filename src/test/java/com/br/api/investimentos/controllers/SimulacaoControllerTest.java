package com.br.api.investimentos.controllers;

import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.models.Simulacao;
import com.br.api.investimentos.repositories.InvestimentosRepository;
import com.br.api.investimentos.services.InvestimentosService;
import com.br.api.investimentos.services.SimulacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTest {
    Simulacao simulacao;
    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void Inicialize(){
         simulacao  = new Simulacao(1,100);
    }
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    SimulacaoService simulacaoService;

    @MockBean
    InvestimentosRepository investimentosRepository;


    @Test
    public void testeSimulacaoCalculo() throws Exception{
        Investimentos investimentos = new Investimentos();
        investimentos.setId(1);
        investimentos.setPorcentagem(0.1);
        double resultado = 100;
        simulacao.setInvestimentoId(1);

        Optional<Investimentos> investimentosOptional = Optional.of(investimentos);
        Mockito.when(investimentosRepository.save(Mockito.any(Investimentos.class))).thenReturn(investimentos);
        Mockito.when(investimentosRepository.findById(Mockito.anyInt())).thenReturn(investimentosOptional);


        Mockito.when(simulacaoService.simulacaoCalculo(simulacao)).thenReturn(resultado);


        String json = objectMapper.writeValueAsString(simulacao);
        mockMvc.perform(MockMvcRequestBuilders.post("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
