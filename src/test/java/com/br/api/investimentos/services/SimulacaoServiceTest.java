package com.br.api.investimentos.services;

import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.models.Simulacao;
import com.br.api.investimentos.repositories.InvestimentosRepository;
import com.br.api.investimentos.repositories.SimulacaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTest {
    @MockBean
    InvestimentosRepository investimentosRepository;


    @Autowired
    SimulacaoService simulacaoService;

    @Test
    public void testarSimulacaoCalculo() {

        Investimentos investimentos = new Investimentos();
        investimentos.setId(1);
        investimentos.setPorcentagem(1.1);
        Optional<Investimentos> investimentosOptional = Optional.of(investimentos);
        Mockito.when(investimentosRepository.save(Mockito.any(Investimentos.class))).thenReturn(investimentos);
        Mockito.when(investimentosRepository.findById(Mockito.anyInt())).thenReturn(investimentosOptional);

        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimentoId(1);
        simulacao.setDinheiroAplicado(100);
        simulacao.setMesesDeAplicacao(1);
        Double retorno = simulacaoService.simulacaoCalculo(simulacao);
        Assertions.assertEquals(simulacao.getResultadoSimulacao(), retorno);

        simulacao.setDinheiroAplicado(90);
        try {
            simulacaoService.simulacaoCalculo(simulacao);
        } catch ( Exception ex) {

            Assertions.assertEquals(ex.getMessage().toString(), "O Dinheiro aplicado deverá ser maior igual a 100");
        }


    }
}
