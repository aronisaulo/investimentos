package com.br.api.investimentos.repositories;

import com.br.api.investimentos.models.Investimentos;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentosRepository  extends CrudRepository<Investimentos, Integer> {
}
