package com.br.api.investimentos.repositories;

import com.br.api.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer>{
}
