package com.br.api.investimentos.controllers;

import com.br.api.investimentos.models.Simulacao;
import com.br.api.investimentos.services.InvestimentosService;
import com.br.api.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;

@RestController
@RequestMapping("simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PutMapping
    public ResponseEntity<Double> simulacaoCalculo(@RequestBody @Valid Simulacao simulacao) {
        double resultado = 0;
        try {
            resultado = simulacaoService.simulacaoCalculo(simulacao);
            return ResponseEntity.status(200).body(resultado);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
}
