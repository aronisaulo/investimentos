package com.br.api.investimentos.controllers;

import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.services.InvestimentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("investimentos")
public class InvestimentosController {

    @Autowired
    private InvestimentosService investimentosService;

    @PostMapping
    public ResponseEntity<Investimentos> criarInvestimentos(@RequestBody @Valid Investimentos investimentos){

        try {
            Investimentos resultado = investimentosService.salvarInvestimentos(investimentos);
            return ResponseEntity.status(201).body(resultado);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

    }

    @GetMapping
    public ResponseEntity<Iterable> buscarInvestimentos(){
        Iterable<Investimentos>  investimentosIterator = investimentosService.buscarTodoInvestimentos();
        return ResponseEntity.status(200).body(investimentosIterator);

    }

    @DeleteMapping("/{id}")
    public Investimentos deletarInvestimentos(@PathVariable Integer id){
        Optional<Investimentos> investimentosOptional =investimentosService.buscarPorId(id);
        if(investimentosOptional.isPresent()){
            investimentosService.deletarInvestimentos(investimentosOptional.get());
            return investimentosOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

}
