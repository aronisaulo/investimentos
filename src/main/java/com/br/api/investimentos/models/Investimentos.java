package com.br.api.investimentos.models;

import com.br.api.investimentos.enums.TipoDeRisco;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
public class Investimentos {
    @NotBlank()
    private String nome;
    @NotBlank()
    private String descricao;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @NotNull

    private TipoDeRisco risco;
    @NotNull
    private double porcentagem;

    public Investimentos() {
    }

    public Investimentos(String nome, String descricao, TipoDeRisco risco, double porcentagem) {
        this.nome = nome;
        this.descricao = descricao;
        this.risco = risco;
        this.porcentagem = porcentagem;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoDeRisco getRisco() {
        return risco;
    }

    public void setRisco(TipoDeRisco risco) {
        this.risco = risco;
    }

    public double getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(double porcentagem) {
        this.porcentagem = porcentagem;
    }
}
