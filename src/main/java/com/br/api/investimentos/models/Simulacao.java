package com.br.api.investimentos.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Simulacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int investimentoId;

    @NotNull
    private int mesesDeAplicacao;

    @NotNull
    private double dinheiroAplicado;
    private double resultadoSimulacao;

    public double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }

    public Simulacao() {
    }

    public Simulacao(@NotNull int mesesDeAplicacao, @NotNull double dinheiroAplicado) {
        this.mesesDeAplicacao = mesesDeAplicacao;
        this.dinheiroAplicado = dinheiroAplicado;
    }

    public int getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(int investimentoId) {
        this.investimentoId = investimentoId;
    }

    public int getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(int mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}

