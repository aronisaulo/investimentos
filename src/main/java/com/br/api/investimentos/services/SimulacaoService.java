package com.br.api.investimentos.services;

import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.models.Simulacao;
import com.br.api.investimentos.repositories.InvestimentosRepository;
import com.br.api.investimentos.repositories.SimulacaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.procedure.ParameterMisuseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class SimulacaoService {

    @Autowired
    InvestimentosRepository investimentosRepository  ;
    @Autowired
    SimulacaoRepository simulacaoRepository;

    public double simulacaoCalculo(Simulacao simulacao){
        if (simulacao.getDinheiroAplicado() >= 100.0) {
            Optional<Investimentos> investimentosOptional = investimentosRepository.findById(simulacao.getInvestimentoId());
            double resultado = calculo(simulacao.getDinheiroAplicado(), investimentosOptional.get().getPorcentagem(), simulacao.getMesesDeAplicacao());
            simulacao.setResultadoSimulacao(resultado);
            return simulacao.getResultadoSimulacao();
        }
        throw new ParameterMisuseException("O Dinheiro aplicado deverá ser maior igual a 100");
    }


    private double calculo(double valor , double percentual , Integer meses){
        double resultado = valor ;
        for (int v = 0 ; v < meses ; v++) {
            resultado += (resultado * percentual) / 100;

        }
        return resultado  ;
    }


}
