package com.br.api.investimentos.services;

import com.br.api.investimentos.models.Investimentos;
import com.br.api.investimentos.models.Simulacao;
import com.br.api.investimentos.repositories.InvestimentosRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Service
public class InvestimentosService {
    @Autowired
    InvestimentosRepository investimentosRepository  ;

    public Investimentos salvarInvestimentos(Investimentos investimentos){
        if (investimentos.getPorcentagem()>0.00)
           return investimentosRepository.save(investimentos);
        else
            throw new ObjectNotFoundException(Investimentos.class, "A porcentagem deverá ser superior a 0.0%");
    }

    public Iterable<Investimentos> buscarTodoInvestimentos(){
        Iterable<Investimentos> investimentos = investimentosRepository.findAll();
        return investimentos;
    }

    public Optional<Investimentos> buscarPorId(int id){
        Optional<Investimentos> investimentosOptional = investimentosRepository.findById(id);
        return investimentosOptional;
    }

    public void deletarInvestimentos(Investimentos investimentos){

        investimentosRepository.delete(investimentos);
    }

}
